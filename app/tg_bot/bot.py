import string
import random
import secrets
import telebot
from telebot import types

with open("/run/secrets/capybara/capybara_token", "r") as file:
    token = file.read().strip()
bot = telebot.TeleBot(token)


def gen_passwd(alphabet: str, add_special_chars: bool, passwd_len: int) -> str:
    # generates a random password, consisting of letters, digits,
    # and maybe special characters. The password must contain at least
    # one lowercase letter, at least one uppercase letter and at least three digits.
    while True:
        passwd = ""
        for i in range(passwd_len):
            passwd += secrets.choice(alphabet)
        if (
            any(char.islower() for char in passwd)
            and any(char.isupper() for char in passwd)
            and sum(char.isdigit() for char in passwd) >= 3
        ):
            if add_special_chars:
                if sum(char in string.punctuation for char in passwd) == 2:
                    break
            else:
                break

    return passwd


def roll(dices) -> str:
    res = 0
    arr = dices.split("d")
    dice_quatnity = int(arr[0])
    faces = int(arr[1])
    roll_events = [face + 1 for face in range(0, faces)]
    while dice_quatnity > 0:
        res += random.choice(roll_events)
        dice_quatnity -= 1
    return res


@bot.message_handler(commands=["start"])
def start_message(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    item1 = types.KeyboardButton("letters+digits")
    item2 = types.KeyboardButton("letters+digits+special_chars")
    item3 = types.KeyboardButton("weather")
    markup.add(item1, item2, item3)
    bot.send_message(
        message.chat.id,
        "Use the buttons to generate a password",
        reply_markup=markup,
    )


@bot.message_handler(commands=["dnd"])
def start_message(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    item5 = types.KeyboardButton("3d6")
    item6 = types.KeyboardButton("d20")
    markup.add(item5, item6)
    bot.send_message(
        message.chat.id,
        "Roll your fate",
        reply_markup=markup,
    )


@bot.message_handler(content_types="text")
def message_reply(message):
    if message.text == "letters+digits":
        bot.send_message(
            message.chat.id,
            gen_passwd(
                string.ascii_letters + string.digits,
                add_special_chars=False,
                passwd_len=14,
            ),
        )
    elif message.text == "letters+digits+special_chars":
        bot.send_message(
            message.chat.id,
            gen_passwd(
                string.ascii_letters + string.digits + string.punctuation,
                add_special_chars=True,
                passwd_len=14,
            ),
        )
    elif message.text == "weather":
        with open("/usr/src/app/weather/weather.txt", "r", encoding="utf-8") as file:
            weather = file.read()
        bot.send_message(message.chat.id, weather)
    elif message.text == "d20":
        bot.send_message(message.chat.id, roll(f"1{message.text}"))
    elif sum(char.isdigit() for char in message.text) >= 2 and ("d" in message.text):
        bot.send_message(message.chat.id, roll(str(message.text)))


if __name__ == "__main__":
    bot.infinity_polling()
