from sys import exit
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.service import Service
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from pyvirtualdisplay import Display


def initiate_display(body) -> None:
    """
    Initiate driver and display,
    close after execution
    """

    def selenium_and_display_settings(*args, **kwargs) -> None:
        display = Display(visible=0, size=(800, 600))
        options = webdriver.FirefoxOptions()
        service = Service(executable_path="/usr/local/bin/geckodriver")
        service_log_path = "/dev/null"
        options.add_argument("--headless")  # turn off display for docker
        driver = webdriver.Firefox(
            options=options, service=service, service_log_path=service_log_path
        )
        # parser's target
        url = "https://yandex.ru/pogoda/ru-RU/moscow/details?lat=55.80794357615003&lon=37.822795413136696"
        print("start display")
        display.start()
        print("get url")
        driver.get(url)
        print("run function")
        body(driver, *args, **kwargs)
        driver.quit()
        display.stop()

    return selenium_and_display_settings


@initiate_display
def parse(driver) -> None:
    """print weather"""
    print("start parsing")
    WebDriverWait(driver, 15).until(
        EC.presence_of_element_located(
            (By.XPATH, "/html/body/div[1]/div/div/div[1]/main/div[2]/article[1]/div[1]")
        )
    )  # wait weather's load
    try:
        morning = driver.find_element(
            by=By.XPATH,
            value="/html/body/div[1]/div/div/div[1]/main/div[2]/article[1]/div[1]",
        ).text
        afternoon = driver.find_element(
            by=By.XPATH,
            value="/html/body/div[1]/div/div/div[1]/main/div[2]/article[1]/div[7]",
        ).text
        evening = driver.find_element(
            by=By.XPATH,
            value="/html/body/div[1]/div/div/div[1]/main/div[2]/article[1]/div[13]",
        ).text
        night = driver.find_element(
            by=By.XPATH,
            value="/html/body/div[1]/div/div/div[1]/main/div[2]/article[1]/div[19]",
        ).text
    except NoSuchElementException as e:
        print(driver.page_source)
        print(e)
        exit(1)
    res = f"{morning}\n{afternoon}\n{evening}\n{night}"
    print(res)
    with open("weather/weather.txt", "w", encoding="utf-8") as file:
        file.write(res)


if __name__ == "__main__":
    print("start app")
    parse()
